import React, { Fragment, useState } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom';

import 'bootstrap/dist/css/bootswatch.min.css';
import './App.css';

import Header from './Components/Header';
import Transaction from './Screens/Transaction';
import TransactionHistory from './Screens/TransactionHistory';

function App() {

    let customerName = "Michael Quitorio";
    const [ transactions, setTransactions ] = useState(
        [
            {
                date: "March 20, 2020",
                amount: 200.00
            },
            {
                date: "March 21, 2020",
                amount: 300.00
            },
            {
                date: "March 22, 2020",
                amount: 400.00
            }
        ]
    );

    const handleAddTransaction = ( {amount} ) => {
        setTransactions(
            [
                ...transactions,
                {
                    date: Date().toString(),
                    amount: parseFloat(amount)
                }
            ]
        );
    };

    return(
        <Fragment>
            <Header customerName={ customerName } />
            <Router>
                <Switch>
                    <Route path="/transaction/history">
                        <TransactionHistory transactions={ transactions } />
                    </Route>
                    <Route path="/transaction">
                        <Transaction onNewTransaction={ handleAddTransaction } />
                    </Route>
                    <Route path="/">
                        <div className="jumbotron">
                            <div className="container">
                                <h1 className="display-3">Hello, world!</h1>
                                <p className="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                                <hr className="my-4" />
                                <p>It uses utility class for typography and spacing to space content out within the larger container.</p>
                                <p className="lead">
                                    <Link className="btn btn-outline-primary mr-5" to="/transaction" role="button">Transaction</Link>
                                    <Link className="btn btn-outline-primary" to="/transaction/history" role="button">History</Link>
                                </p>
                            </div>
                        </div>
                    </Route>
                </Switch>
            </Router>
        </Fragment>
    );
}

export default App;