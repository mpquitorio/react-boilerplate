import React from 'react';
import Table from '../Components/Table';

const TransactionHistory = (props) => {   
    return(
        <Table transactions={ props.transactions } />
    );
}

export default TransactionHistory;