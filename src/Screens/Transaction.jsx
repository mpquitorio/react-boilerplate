import React, { Fragment, useState } from 'react';

import CurrentBalance from '../Components/CurrentBalance';
import TransactionForm from '../Components/TransactionForm';

const Transaction = ( {onNewTransaction} ) => {

    let [ currentBalance, setCurrentBalance ] = useState(0);
    let [ amount, setAmount ] = useState(0);

    const withdrawFunds = () => {
        let newCurrentBalance = currentBalance - amount;
        setCurrentBalance(newCurrentBalance);
    
        onNewTransaction(
            {
                newBalance: newCurrentBalance,
                amount: amount,
                type: "withdraw"
            }
        );

        setAmount(0);
    };
    
    const depositFunds = () => {
        let newCurrentBalance = currentBalance + amount;
        setCurrentBalance(newCurrentBalance);

        onNewTransaction(
            {
                newBalance: newCurrentBalance,
                amount: amount,
                type: "deposit"
            }
        );

        setAmount(0);
    };

    return(
        <Fragment>
            <div className="container">
                <div className="row">
                    <div className="col text-center">
                        <CurrentBalance balance={ currentBalance } />
                        <TransactionForm
                            onWithdrawFunds={ withdrawFunds }
                            onDepositFunds={ depositFunds }
                            amount={ amount }
                            setAmount={ setAmount }
                        />
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default Transaction;