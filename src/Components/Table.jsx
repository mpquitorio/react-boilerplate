import React from 'react';

//You can exclude .jsx extension because React assumes that the file ends with JS/JSX extension
import TableRow from './TableRow';

const Table = (props) => {
    return(
        <div className="container">
            <table className="table mt-3">
                <thead className="text-center">
                    <tr>
                        <th>Date</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody className="text-center">
                    {props.transactions.map((transaction, index) => {
                        return(
                            <TableRow 
                                key={index} 
                                date={transaction.date} 
                                amount={transaction.amount} 
                            />
                        );
                    })}

                    {/* This is an alternative of .map() for generating JSX dynamically using forEach */}
                    {/* {transactionsJSX} */}

                </tbody>
            </table>
        </div>
    );
}

export default Table;