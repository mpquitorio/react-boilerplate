import React from 'react';

const Input = (props) => {
    return (
        <div className="form-group px-5">
            <label>{ props.label }</label>
            <input
                className="form-control"
                type={ props.type }
                value={ props.value }
                onChange={ props.onChange }
            />
        </div>
    );
}

export default Input;