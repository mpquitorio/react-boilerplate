import React from 'react';

const Button = (props) => {
    return(
        <button className="btn btn-primary mx-1" onClick={ props.onPress }>
            { props.label }
        </button>
    );
}

export default Button;