import React from 'react';

const TableRow = (props) => {
    return (
        <tr>
            <td>{ props.date }</td>
            <td>{ "P" + props.amount.toFixed(2) }</td>
        </tr>
    );
}

export default TableRow;