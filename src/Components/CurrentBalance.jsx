import React from 'react';

const CurrentBalance = ( {balance} ) => {
    
    function number_format(num) {
        let n = num.toString().split(".");
        n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return n.join(".");
    }
    
    return (
        <div className="mb-5 py-5" id="currentBalance">
            Php { number_format(balance.toFixed(2)) }
        </div>
    );
}

export default CurrentBalance;