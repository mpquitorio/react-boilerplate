import React, { Fragment } from 'react';
import { 
    Link 
} from 'react-router-dom';

import Input from './Input';
import Button from './Button';

const TransactionForm = ( {amount, setAmount, onWithdrawFunds, onDepositFunds} ) => {
    return(
        <Fragment>
            <Input
                label="Amount: "
                type="number"
                value={amount}
                onChange={ 
                    ( {target: {value: newAmount}} ) => {
                        setAmount(parseFloat(newAmount));
                    }
                }
            />

            <Button label="Withdraw" onPress = { onWithdrawFunds } />
            <Button label="Deposit" onPress = { onDepositFunds } />
            <Link to="/transaction/history" className="btn btn-primary mx-1">View Transaction History</Link>
        </Fragment>
    );
}

export default TransactionForm;