import React, { useState } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    NavbarText
  } from 'reactstrap';

const Header = ( {customerName} ) => {

    const [ isOpen, setIsOpen ] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    return (
        <Navbar className="navbar navbar-expand-lg navbar-dark bg-primary">
            <div className="container">
                <NavbarBrand href="/" className="navbar-brand">ATM Transaction</NavbarBrand>
                <NavbarToggler className="navbar-toggler" onClick={toggle} />
                <Collapse className="navbar-collapse collapse" isOpen={isOpen}>
                    <Nav className="navbar-nav mr-auto">
                        <NavItem className="nav-item">
                            <NavLink className="nav-link" href="/transaction">Transaction</NavLink>
                        </NavItem>
                        <NavItem className="nav-item">
                            <NavLink className="nav-link" href="/transaction/history">History</NavLink>
                        </NavItem>
                    </Nav>
                    <NavbarText>Hi, { customerName }.</NavbarText>
                </Collapse>
            </div>
        </Navbar>
    );
}

export default Header;